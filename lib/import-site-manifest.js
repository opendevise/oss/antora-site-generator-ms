'use strict'

const readSiteManifest = require('./read-site-manifest')
const parseResourceId = require('@antora/content-classifier/lib/util/parse-resource-id')

async function importSiteManifest (playbook, contentCatalog, asciidocConfig, primarySiteManifestUrl, primarySiteUrl) {
  if (!primarySiteManifestUrl) primarySiteManifestUrl = primarySiteUrl + '/site-manifest.json'
  const { components, url: siteUrlFromManifest } = await readSiteManifest(playbook, primarySiteManifestUrl)
  if (!components) return
  if (!primarySiteUrl) primarySiteUrl = siteUrlFromManifest
  Object.assign(asciidocConfig.attributes, {
    'primary-site-url': primarySiteUrl,
    'primary-site-manifest-url': primarySiteManifestUrl,
  })
  const primarySite = { url: primarySiteUrl }
  components.forEach(({ name, title, versions }) => {
    const localComponent = contentCatalog.getComponent(name)
    versions.forEach(({ displayVersion, pages, version, url: startUrl }) => {
      let componentVersion = localComponent && contentCatalog.getComponentVersion(localComponent, version)
      let mergeComponentVersion
      if (!componentVersion) {
        const componentVersionDesc = { asciidoc: asciidocConfig, displayVersion, title }
        componentVersion = contentCatalog.registerComponentVersion(name, version, componentVersionDesc)
        componentVersion.site = primarySite
        componentVersion.url = primarySiteUrl + startUrl
      } else if (componentVersion.name === 'home') {
        mergeComponentVersion = true
      } else {
        // NOTE don't import component version if already in this site (unless component is home)
        return
      }
      pages.forEach(({ module: module_ = 'ROOT', path, title: doctitle, url }) => {
        const pageId = { component: name, version, module: module_, relative: path, family: 'page' }
        // NOTE don't overwrite page if it's in the current site
        if (mergeComponentVersion && contentCatalog.getById(pageId)) return
        contentCatalog.addFile({
          asciidoc: { xreftext: doctitle },
          site: primarySite,
          out: undefined,
          pub: { url: primarySiteUrl + url },
          src: pageId,
        })
      })
    })
    if (!localComponent) contentCatalog.getComponent(name).site = primarySite
  })
  // NOTE ignore alias for a location in the primary site
  override(
    contentCatalog,
    'registerPageAlias',
    (super_) =>
      function (spec, rel) {
        const src = parseResourceId(spec, rel.src, 'page', ['page'])
        if (!src) return
        const component = this.getComponent(src.component)
        if (component) {
          if ((this.getComponentVersion(component, src.version || component.latest.version) || {}).site) return
        }
        return super_.apply(this, arguments)
      }
  )
}

function override (object, methodName, cb) {
  object[methodName] = cb(object[methodName])
}

module.exports = importSiteManifest
