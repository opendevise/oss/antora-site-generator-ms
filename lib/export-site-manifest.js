'use strict'

const createSiteFile = require('./create-site-file')

function exportSiteManifest (components, allPages, allAliases, siteUrl, relativePath = 'site-manifest.json') {
  components = [...components].sort(({ name: nameA }, { name: nameB }) => nameA.localeCompare(nameB))
  const pagesByComponentVersion = allPages.reduce((accum, { title, pub, src }) => {
    const key = `${src.version}@${src.component}`
    const entries = accum[key] || (accum[key] = [])
    entries.push({ module: src.module, path: src.relative, url: pub.url, title })
    return accum
  }, {})
  allAliases.reduce((accum, { src, rel: { title, pub } }) => {
    const key = `${src.version}@${src.component}`
    const entries = accum[key] || (accum[key] = [])
    entries.push({ alias: true, module: src.module, path: src.relative, url: pub.url, title })
    return accum
  }, pagesByComponentVersion)
  const manifestData = {
    version: 3,
    generated: +new Date(),
    url: siteUrl,
    components: components
      .map(({ name, title, versions, latest: { version: latest } }) => ({
        name,
        title,
        latest,
        versions: versions
          .filter(({ site }) => !site)
          .reduce((accum, { version, displayVersion, url }) => {
            const pages = pagesByComponentVersion[`${version}@${name}`]
            // NOTE don't add version if there are no pages
            if (!pages) return
            pages.sort(
              ({ module: moduleA, path: pathA }, { module: moduleB, path: pathB }) =>
                moduleA.localeCompare(moduleB) || pathA.localeCompare(pathB)
            )
            if (displayVersion === version) displayVersion = undefined
            return accum.concat({ version, displayVersion, url, pages })
          }, []),
      }))
      .filter((component) => component.versions),
  }
  return createSiteFile(relativePath, JSON.stringify(manifestData, null, 2), 'application/json')
}

module.exports = exportSiteManifest
