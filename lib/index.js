'use strict'

/**
 * Antora Site Generator for MS
 *
 * Runs an Antora pipeline using the default set of components with a focus on producing and publishing a documentation
 * site. In additional to the default behavior, it resolves unsatisfied xrefs in a microsite to those in a primary site.
 */
module.exports = require('./generate-site')
