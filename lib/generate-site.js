'use strict'

const aggregateContent = require('@antora/content-aggregator')
const buildNavigation = require('@antora/navigation-builder')
const buildPlaybook = require('@antora/playbook-builder')
const classifyContent = require('@antora/content-classifier')
const convertDocuments = require('@antora/document-converter')
const create404 = require('./create-404')
const createPageComposer = require('@antora/page-composer')
const exportSiteManifest = require('./export-site-manifest')
const exportSiteNavigationData = require('./export-site-navigation-data')
const fragmentizeContents = require('./fragmentize-contents')
const importSiteManifest = require('./import-site-manifest')
const loadUi = require('@antora/ui-loader')
const mapSite = require('@antora/site-mapper')
const produceRedirects = require('@antora/redirect-producer')
const publishSite = require('@antora/site-publisher')
const { resolveConfig: resolveAsciiDocConfig } = require('@antora/asciidoc-loader')

async function generateSite (args, env) {
  const playbook = buildPlaybook(args, env)
  const asciidocConfig = resolveAsciiDocConfig(playbook)
  const asciidocAttrs = asciidocConfig.attributes
  const [contentCatalog, uiCatalog] = await Promise.all([
    aggregateContent(playbook).then((contentAggregate) => classifyContent(playbook, contentAggregate, asciidocConfig)),
    loadUi(playbook),
  ])
  const primarySiteUrl = asciidocAttrs['primary-site-url']
  const primarySiteManifestUrl = asciidocAttrs['primary-site-manifest-url']
  if (primarySiteUrl || primarySiteManifestUrl) {
    await importSiteManifest(playbook, contentCatalog, asciidocConfig, primarySiteManifestUrl, primarySiteUrl)
  }
  const pages = convertDocuments(contentCatalog, asciidocConfig)
  if (asciidocAttrs['site-fragmentizer'] != null) {
    pages.forEach((page) => {
      const queries = ((page.asciidoc || {}).attributes || {})['page-fragmentize']
      if (queries != null) page.contents = fragmentizeContents(page, queries)
    })
  }
  const navigationCatalog = buildNavigation(contentCatalog, asciidocConfig)
  const composePage = createPageComposer(playbook, contentCatalog, uiCatalog, env)
  pages.forEach((page) => composePage(page, contentCatalog, navigationCatalog))
  const siteFiles = mapSite(playbook, pages).concat(produceRedirects(playbook, contentCatalog))
  const localComponents = contentCatalog.getComponents().filter(({ site }) => !site)
  if (playbook.site.url) siteFiles.push(composePage(create404()))
  siteFiles.push(
    exportSiteManifest(
      localComponents,
      pages,
      contentCatalog.findBy({ family: 'alias' }),
      playbook.site.url,
      asciidocAttrs['site-manifest-path']
    )
  )
  const siteNavigationDataPath = asciidocAttrs['site-navigation-data-path']
  if (siteNavigationDataPath || 'export-site-navigation-data' in asciidocAttrs) {
    siteFiles.push(
      exportSiteNavigationData(localComponents, asciidocAttrs['site-component-order'], siteNavigationDataPath)
    )
  }
  const siteCatalog = { getAll: () => siteFiles }
  return publishSite(playbook, [contentCatalog, uiCatalog, siteCatalog])
}

module.exports = generateSite
